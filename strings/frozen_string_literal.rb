# frozen_string_literal: true

# The line above is a magic comment. 'frozen_string_literal: true' improves application performance by not allocating
# new space for the same string, thereby also saving time for garbage collection chores. How? when you freeze a string
# literal(string object), you're telling Ruby to not let any of your programs modify the string literal (object).

def frozen_str
  'test'
end

# prints same id because by freezing string literals, you're not allocating new memory space for it.
# .equal? checks if the two operands refer to the same object
# == checks if the two operands have the same value
puts frozen_str.equal? frozen_str # true
puts frozen_str == frozen_str     # true

# If you want a string literal to be mutable regardless of the global or per-file setting, you can prefix it with the
# unary + operator
def no_frozen_str
  +frozen_str
end

# If you want a string literal to be mutable regardless of the global or per-file setting, you can call .dup on it
def no_frozen_str2
  frozen_str.dup
end

# If you want a string literal to be mutable regardless of the global or per-file setting, you can call .clone on it
def no_frozen_str3
  frozen_str.clone
end

# .duv vs .clone (https://www.rubyguides.com/2018/11/dup-vs-clone)
# Both methods copy an object, the difference is that .dup doesn’t copy the object attributes:
# - frozen status
# - tainted status
# - singleton class

puts no_frozen_str.equal? no_frozen_str # false
puts no_frozen_str == no_frozen_str     # true

puts no_frozen_str3.equal? no_frozen_str3 # false
puts no_frozen_str3 == no_frozen_str3     # true

puts frozen_str.frozen?     # true
puts no_frozen_str.frozen?  # false
puts no_frozen_str2.frozen? # false
puts no_frozen_str3.frozen? # true

# GitLab uses the the magic comment `# frozen_string_literal: true` in its files and override it by using `+""` sintax
# and `.dup`. E.g.:
# +"": https://gitlab.com/gitlab-community/gitlab/-/blob/04b9de85a8355a9e9f4827fcd608c3ea2bcd7df0/lib/gitlab/gitaly_client/repository_service.rb#L338
# .dup: https://gitlab.com/gitlab-community/gitlab/-/blob/a6bddf15155c514e5dc826ee04da79993dcff3bd/app/models/license_template.rb#L44
