def frozen_str_id
  'test'.freeze
end

# prints diferent ids because by freezing string literals, you're not allocating new memory space for it.
# .equal? checks if the two operands refer to the same object
# == checks if the two operands have the same value
puts frozen_str_id.equal? frozen_str_id
puts frozen_str_id == frozen_str_id
