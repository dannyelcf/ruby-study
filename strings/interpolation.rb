# frozen_string_literal: true

puts '2 + 2 = #{2 + 2}' # 2 + 2 = #{2 + 2}
# Use double quoted strings if you need interpolation
puts "2 + 2 = #{2 + 2}" # 2 + 2 = 4
# Escaping `#` in a double quote literal string
puts "2 + 2 = \#{2 + 2}" # 2 + 2 = #{2 + 2}
