# character literal
puts 'a'   # a
puts "\n"  # \n
puts "\s"  # \a

# Prefer single-quoted strings when you don't need string interpolation or special symbols.
puts 'Hello'.downcase.include? 'h'

'Hello'.each_char { |c| puts c }
'Hello'.each_char.with_index { |c, i| puts "c[#{i}]=#{c}" }
